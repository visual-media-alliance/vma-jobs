<?php
/**
 * Plugin Name: WordPress Sentry
 * Plugin URI: https://github.com/stayallive/wp-sentry
 * Description: A (unofficial) WordPress plugin to report PHP and JavaScript errors to Sentry.
 * Version: must-use-proxy
 * Author: Alex Bouma
 * Author URI: https://alex.bouma.dev
 * License: MIT
 */


if (isset(ENV['sentry_error_types'])) {
    define('WP_SENTRY_ERROR_TYPES', ENV['sentry_error_types']);
}

if (isset(ENV['sentry_php_dsn'])) {
    define('WP_SENTRY_PHP_DSN', ENV['sentry_php_dsn']);
}

if (isset(ENV['sentry_browser_dsn'])) {
    define('WP_SENTRY_BROWSER_DSN', ENV['sentry_browser_dsn']);
}

if (isset(ENV['sentry_env'])) {
    define('WP_SENTRY_ENV', ENV['sentry_env']);
}



function wp_sentry_clientbuilder_callback( \Sentry\ClientBuilder $builder ): void {
    // For example, disabling the default integrations 
    $builder->getOptions()->setCaptureSilencedErrors(True);
}

define( 'WP_SENTRY_CLIENTBUILDER_CALLBACK', 'wp_sentry_clientbuilder_callback' );

$wp_sentry = __DIR__ . '/../plugins/wp-sentry-integration/wp-sentry.php';

if (! file_exists($wp_sentry)) {
	return;
}

require $wp_sentry;

define('WP_SENTRY_MU_LOADED', true);
