<?php
/**
 * Plugin Name: WP local development env setup
 * Description: Adds dev features when wp env is set to local 
 */

/**
 * Bail out if this is not a test or production env
 */
if (!in_array(wp_get_environment_type(), ['local'])) {
    return;
}


add_action('init', function() {
    /**
     * Hook wp_mail() to Use mailhog in local dev environment 
     * 
     * Setup the local environment to use the mailhog configuration
     * provided in the vsocde .devcontainer setup
     */
    add_action('phpmailer_init', function ($phpmailer) {
        /**
         * wp_mail() reconfigures phpmailer on every call,
         * set it back to what we want here in this hook
         * 
         */
        if (wp_get_environment_type() === 'local') {
            $phpmailer->IsSMTP();
            $phpmailer->Host = 'localhost';
            $phpmailer->Port = 1025;
            $phpmailer->SMTPAuth = false;
            $phpmailer->SMTPSecure = '';
        }

        return $phpmailer;
    });
});
