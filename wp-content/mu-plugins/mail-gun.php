<?php
/**
 * Plugin Name: Mail Gun
 * Description: Sets up mail transit
 * Author: Evan Bangham
 */

add_action('phpmailer_init', function ($phpmailer) {
    /**
     * wp_mail() reconfigures phpmailer on send,
     * set it back to what we want here in this hook
     * 
     */
    $phpmailer->isSMTP();  // Set mailer to use SMTP
    $phpmailer->Host = 'smtp.mailgun.org';  // Specify mailgun SMTP servers
    $phpmailer->Port = ENV['smtp_port'];
    $phpmailer->SMTPAuth = true; // Enable SMTP authentication
    $phpmailer->Username = ENV['smtp_user']; // SMTP username from https://mailgun.com/cp/domains
    $phpmailer->Password = ENV['smtp_pass']; // SMTP password from https://mailgun.com/cp/domains
    $phpmailer->SMTPSecure = 'tls';   // Enable encryption, 'ssl'

    return $phpmailer;
});