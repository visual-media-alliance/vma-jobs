<!-- Header -->
<header id="header">
	<!-- Top -->
	<div class="top hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-9 no-padding-right">
					<div class="top-left-block">
						<?php wp_nav_menu( array('menu' => 'Top Left Menu', 'menu_class' => 'top-left-menu')); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 no-padding-left">
					<div class="top-right-block">
						<?php #wp_nav_menu( array('menu' => 'Top Right Menu', 'menu_class' => 'top-right-menu')); ?>
						<?php get_template_part('templates/menu', 'top-right'); ?>				
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Top -->

	<!-- Logo & Nav Banner -->
	<div class="logonav">
		<div class="container relative">
			<div class="row">
				<div class="col-xs-12 col-sm-4 no-padding-right">
					<a href="<?php echo get_site_url(); ?>/" class="logo-link">
						<div class="logo"></div>
					</a>
					<div class="clear"></div>
				</div>
				<div class="col-xs-12 col-sm-8 no-padding" >
					<div role="navigation" class="navbar navbar-default desktop-nav">

						<div class="navbar-header">
							<button type="button" class="navbar-toggle animated-normal" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only animated-normal">Toggle navigation</span> <span class="icon-bar animated-normal"></span> <span class="icon-bar animated-normal"></span> <span class="icon-bar animated-normal"></span>
							</button>
						</div>
						<div class="navbar-collapse collapse hidden-xs no-padding-left">
							<?php wp_nav_menu( array('container' => 'navbar-collapse collapse', 'menu' => 'Main Menu', 'menu_class' => 'nav navbar-nav')); ?>
							<div class="clear"></div>
						</div>

					</div>

					<div class="navbar navbar-default mobile-nav hidden-lg hidden-md hidden-sm" >
						<div class="navbar-collapse collapse" >
							<div class="scroller relative">
								<div class="top-close-block">
									<button type="button" class="close-btn" data-toggle="collapse" data-target=".navbar-collapse">
										<span class="icon-bar top-bar animated-normal"></span>
										<span class="icon-bar bottom-bar animated-normal"></span>
									</button>
									<div class="clear"></div>
								</div>
								<?php wp_nav_menu( array('menu' => 'Main Menu', 'menu_class' => 'nav navbar-nav')); ?>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- Logo & Nav Banner -->
	
</header>
<!-- Header -->

<?php

$banner_background 			= get_stylesheet_directory_uri().'/images/hero-background.jpg';

$title = "VMA Career Center";
if (is_archive()) {
	if (is_post_type_archive()) {
		$post_type = get_query_var('post_type');
		switch ($post_type) {
			case 'job_listing':
				$title = 'Find a Job';
				break;
			case 'resume':
				$title = 'Search Resumes';
				break;
		}
	}
	else {
		$title = get_the_archive_title();
	}

}
elseif (is_singular()) {
	$title = get_the_title();
}

 ?>

<div class="section section-banner section-parallax" data-parallax="scroll" data-image-src="<?php echo $banner_background;?>" data-bleed="0" data-position="center">
    <div class="container">
        <div class="col-xs-12 no-padding">            
			<div class="title-block text-center fade-scroll">
				<h1 class="h1 text-bold"><?= $title ?></h1>
			</div>
        </div>
		<div class="clear" ></div>
    </div>
</div>