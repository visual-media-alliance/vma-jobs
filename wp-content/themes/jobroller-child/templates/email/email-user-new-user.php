<?php

global $supportContact;
$support = $supportContact;

?>

<p>Hello <?= $args['user']->user_login; ?></p>

<p>Thank you for signing up for the Visual Media Alliance (VMA) Job Center. Here's how to get started on the site:</p>

<p>Are you a job seeker? Create a profile, submit your resume, and search and apply for jobs, right
from the website.</p>

<p>Are you an employer? To post an open position, contact us to upgrade your profile.</p>

<p>Click <a href="<?=  wp_login_url(); ?>">here</a> to log in again.</p>

<p>
    Having trouble with the website? Please contact <?= $support['name'] ?> at 
    <?= $support['email'] ?> or <?= $support['phone'] ?>.
</p>