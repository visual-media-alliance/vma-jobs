
<p>A password reset was requested for the following VMA Job Center account:</p>

<p>Username: <?= $args['user_login']?></p>

<p>If this was a mistake, ignore this email and nothing will happen. To reset your password, visit
the following address: <a href="<?= $args['link'] ?>">address</a></p>