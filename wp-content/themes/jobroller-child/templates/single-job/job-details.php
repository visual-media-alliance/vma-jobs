<?php

$args = wp_parse_args($args, [
        'compensation' => null,
        'type' => null,
        'categories' => [],
        'company-name' => '',
        'company-url' => '',
        'job-tags' => [],
]);

$compensation = $args['compensation'];
$type = $args['type'];
$categories = $args['categories'];
?>

<div class="job-details">
    <h2>Job Details</h2>
    <?php if ($args['company-name']): ?>
        <h3>Company</h3>
        <p>
            <?php if ($args['company-url']): ?>
                <a href="<?= $args['company-url'] ?>" target="_blank">
                    <?= $args['company-name'] ?>
                </a> 
                <i class="fa fa-external-link"></i>
            <?php else: ?>
                <?= $args['company-name'] ?>
            <?php endif; ?>
        </p>
    <?php endif; ?>
    <?php if ($args['type']): ?>
        <h3>Job Type</h3>
        <p><?= $type->name ?></p>
        <?php endif; ?>
        <?php if ($compensation): ?>
        <h3>Job Compensation</h3>
            <p><?= $compensation->name ?></p>
    <?php endif; ?>
</div>