<?php
$dashboard = get_post(JR_Dashboard_Page::get_id());
$login = get_post(APP_Login::get_id());
?>

<div class="menu-top-right-menu-container">
    <ul id="menu-top-right-menu" class="top-right-menu">
    <?php if (is_user_logged_in()): ?>
        <li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44">
        <div class="item-session">
            <a href="<?= get_permalink($dashboard) ?>">
                <i class="dashicons dashicons-admin-users"></i>
                <?= wp_get_current_user()->data->user_email ?>
            </a>
            (<a href="<?= esc_url( wp_logout_url( home_url() ) ); ?>">logout</a>)
        </div>
        </li>
    <?php else: ?>
        <li id="menu-item-44" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44">
            <a href="<?= get_permalink($login) ?>">Login/Register</a>
        </li>
    <?php endif; ?>
    </ul>
</div>	