<?php
$job_submit = get_post(JR_Job_Submit_Page::get_id());
$resume_edit = get_post(JR_Resume_Edit_Page::get_id());
$login = get_post(APP_Login::get_id());
$dashboard = get_post(JR_Dashboard_Page::get_id());
$profile = get_post(JR_User_Profile_Page::get_id());
?>

<?php if ($args['display'] === 'dual'): ?>
<li 
	id="menu-item-63"
	class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-63"
>
	<a href="<?= site_url() ?>">For Employers</a>
	<ul class="sub-menu">
		<li
			id="menu-item-46"
			class="menu-item menu-item-type-post_type_archive menu-item-object-resume menu-item-46"
		><a href="<?= get_post_type_archive_link('resume') ?>">Search Resumes</a></li>
		<li
			id="menu-item-27"
			class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"
		><a href="<?= get_permalink($job_submit) ?>">Submit Job</a></li>
	</ul>
</li>
<li
	id="menu-item-64"
	class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-64"
>
	<a href="<?= site_url() ?>">For Job Seekers</a>
	<ul class="sub-menu">
		<li
			id="menu-item-25"
			class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
		><a href="<?= get_post_type_archive_link('job_listing') ?>">Find a Job</a></li>
		<li
			id="menu-item-28"
			class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"
		><a href="<?= get_permalink($resume_edit) ?>">Post Resume</a></li>
	</ul>
</li>

<?php elseif($args['display'] === 'job_seeker'): ?>
	<li
		id="menu-item-25"
		class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
	>
		<a href="<?= get_post_type_archive_link('job_listing') ?>">Find a Job</a>
	</li>
	<li
		id="menu-item-64"
		class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-64"
	>
		<a href="<?= get_permalink($dashboard) . '#resumes' ?>">Post Resumes</a>
		<ul class="sub-menu">
			<li
				id="menu-item-25"
				class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
			>
				<a href="<?= get_permalink($dashboard) . '#resumes'?>">Edit Resumes</a>
			</li>
			<li
				id="menu-item-28"
				class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"
			>
				<a href="<?= get_permalink($resume_edit); ?>">New Resume</a>
			</li>
		</ul>
	</li>
<?php elseif($args['display'] === 'job_lister'): ?>
	<li
		id="menu-item-64"
		class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-64"
	>
		<a href="<?= get_permalink($dashboard) ?>">Post Jobs</a>
		<ul class="sub-menu">
			<li
				id="menu-item-25"
				class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
			>
				<a href="<?= get_permalink($dashboard) . '#live'; ?>">Edit Jobs</a>
			</li>
			<li
				id="menu-item-28"
				class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"
			>
				<a href="<?= get_permalink($job_submit) . '#prefs' ?>">New Job</a>
			</li>
		</ul>
	</li>
	<li
		id="menu-item-46"
		class="menu-item menu-item-type-post_type_archive menu-item-object-resume menu-item-46"
	>
		<a href="<?= get_post_type_archive_link('resume') ?>">Search Resumes</a>
	</li>
<?php elseif($args['display'] == 'guest'): ?>
	<li
		id="menu-item-62"
		class="menu-item menu-item-type-custom menu-item-object-custom menu-item-62"
	>
		<a href="<?= get_permalink($login) ?>">Login/Register</a>
	</li>
	<li
		id="menu-item-25"
		class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
	>
		<a href="<?= get_post_type_archive_link('job_listing') ?>">Jobs</a>
	</li>
	<li
		id="menu-item-62"
		class="menu-item menu-item-type-custom menu-item-object-custom menu-item-62"
	>
		<a href="https://main.vma.bz/who-we-are" target="_blank">Who We Are</a>
	</li>
	<li
		id="menu-item-29"
		class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"
	>
		<a href="https://main.vma.bz/contact" target="_blank">Contact</a>
	</li>
<?php endif; ?>
<li
		id="menu-item-29"
		class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"
>
		<a href="<?= site_url() . '/help' ?>">Help</a>
</li>
<?php if (is_user_logged_in()): ?>
	<li
		id="menu-item-64"
		class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-64"
	>
		<a href="<?= get_permalink($dashboard) ?>">Account</a>
		<ul class="sub-menu">
			<li
				id="menu-item-25"
				class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
			>
				<a href="<?= get_permalink($profile); ?>">Edit Profile</a>
			</li>
			<?php 
			if (
					$args['display'] === 'job_seeker' ||
					$args['display'] == 'dual'
				):
			?>
				<li
					id="menu-item-28"
					class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"
				>
					<a href="<?= get_permalink($dashboard) . '#prefs' ?>">Preferences</a>
				</li>
			<?php endif; ?>
			<li
				id="menu-item-25"
				class="menu-item menu-item-type-post_type_archive menu-item-object-job_listing menu-item-25"
			>
				<a href="<?= esc_url( wp_logout_url( home_url() ) ) ?>">Logout</a>
			</li>
		</ul>
	</li>
<?php endif; ?>
