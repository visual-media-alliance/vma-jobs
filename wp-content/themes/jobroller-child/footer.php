
<div class="footer">
	<div class="section section-footer">
	    <div class="container">

	        <div class="row" >

				<?php foreach ( $footer_cols as $number ): ?>
					<?php $sidebar_id = "footer_col_{$number}"; ?>
	                <div class="col-xs-6 col-sm-3">
						<div class="column">
							<div id="<?php echo esc_attr( $sidebar_id ); ?>">
								<?php dynamic_sidebar( $sidebar_id ); ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

				<div class="col-xs-12 col-sm-6">
                    <div class="contact-info">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="footer-logo">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/vma-footer-logo.png" alt=""/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <p class="text-bold">665 Third Street, Suite 500, <br />San Francisco, CA 94107</p>
                                <div class="social-icons hidden-xs">						
                                    <a class="icon icon-facebook" href="https://www.facebook.com/VisualMediaAlliance" target="_blank"></a>
                                    <a class="icon icon-twitter" href="https://twitter.com/visualmediaall" target="_blank"></a>
                                    <a class="icon icon-linkedin" href="https://www.linkedin.com/in/visualmediaalliance" target="_blank"></a>   
                                    <a class="icon icon-google-plus" href="https://plus.google.com/+VisualMediaAllianceSanFrancisco" target="_blank"></a>   
                                    <a class="icon icon-youtube" href="https://www.youtube.com/channel/UCHXmmPqwdDpvKzmoN0NvLew/feed" target="_blank"></a>
                                    <a class="icon icon-flickr" href="https://www.flickr.com/photos/visualmediaalliance" target="_blank"></a>   
                                    <a class="icon icon-pinterest" href="https://www.pinterest.com/visualmediaall" target="_blank"></a>								
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
    							<div class="contact-number">
    								<p>Phone: <a href="tel:800.659.3363">800.659.3363</a></p>
    								<p>Fax: 800.428.1911</p>
    								<p>Email: <a href="mailto:shannon@vma.bz">info@vma.bz</a></p>
    							</div>
                            </div>
    						<div class="col-xs-12 col-sm-6 hidden-lg hidden-md hidden-sm">
    						 	<div class="social-icons">
                                    <a class="icon icon-facebook" href="https://www.facebook.com/VisualMediaAlliance" target="_blank"></a>
                                    <a class="icon icon-twitter" href="https://twitter.com/visualmediaall" target="_blank"></a>
                                    <a class="icon icon-linkedin" href="https://www.linkedin.com/in/visualmediaalliance" target="_blank"></a>   
                                    <a class="icon icon-google-plus" href="https://plus.google.com/+VisualMediaAllianceSanFrancisco" target="_blank"></a>   
                                    <a class="icon icon-youtube" href="https://www.youtube.com/channel/UCHXmmPqwdDpvKzmoN0NvLew/feed" target="_blank"></a>
                                    <a class="icon icon-flickr" href="https://www.flickr.com/photos/visualmediaalliance" target="_blank"></a>   
                                    <a class="icon icon-pinterest" href="https://www.pinterest.com/visualmediaall" target="_blank"></a>
                                    <div class="clear"></div>
                                </div>
    						</div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

				<div class="clear"></div>
	           
	            <div class="copyright text-center">
	                <span>&copy; 1986-<?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?> | <a href="#">Privacy Policy</a>  | <a href="#">Terms & Conditions</a></span>
	            </div>

			</div><!-- end row -->

		</div><!-- container -->

	</div><!-- end footer -->
</div>