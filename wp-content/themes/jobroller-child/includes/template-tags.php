<?php

function vma_the_resume_salary( $text = '', $before = '', $after = '' ) {
	$desired_salary = get_post_meta( get_the_ID(), '_desired_salary', true );

	if ( $desired_salary ) {
		$text = $text ? $text : __( 'Desired salary: ', APP_TD );
		echo $text . $before . $desired_salary . $after;
	}
}
