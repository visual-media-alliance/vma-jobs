<?php


function vma_sidebar_actions() {
	$resume_tax = array( 'resume_specialities', 'resume_groups', 'resume_languages', 'resume_category', 'resume_job_type' );

	if ( is_page( JR_Resume_Edit_Page::get_id() )|| APP_POST_TYPE_RESUME == get_post_type() || is_post_type_archive('resume') || is_tax( $resume_tax ) ) {
		appthemes_load_template( array( 'sidebar-sresume.php', 'includes/sidebar-sresume.php' ) );
	} else {
		appthemes_load_template( array( 'sidebar-sjob.php', 'includes/sidebar-sjob.php' ) );
	}

}