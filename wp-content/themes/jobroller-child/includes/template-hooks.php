<?php


add_filter('jr_job_preview_fields', function($fields) {
    unset($fields['Logo']);
    unset($fields['Tags']);
    return $fields;
});


add_action('job_main_section', function() {
    $post = get_post();

    $args['company-name'] = get_post_meta($post->ID, '_Company', true);
    $args['company-url'] = get_post_meta($post->ID, '_CompanyURL', true);

    $args['compensation'] = (wp_get_post_terms($post->ID, 'job_salary')[0] ?? null);
    $args['type'] = (wp_get_post_terms($post->ID, 'job_type')[0] ?? null);
    $args['categories'] = (wp_get_post_terms($post->ID, 'job_category') ?? null);
    $args['job-tags'] = (wp_get_post_terms($post->ID, 'job_tag') ?? null);
    get_template_part('templates/single-job/job-details', null, $args);
});

add_action('after_setup_theme', function() {
    remove_action('appthemes_before_sidebar_widgets', 'jr_sidebar_sjob');
    add_action('appthemes_before_sidebar_widgets', 'vma_sidebar_actions');
}, 100);




/**
 * Bake the main menu into the theme code
 */
add_filter( 'wp_nav_menu_items', function($items, $args) {

    $get_current_user_roles = function() {
        if( is_user_logged_in() ) {
            $user = wp_get_current_user();
            $roles = ( array ) $user->roles;
            return $roles;
        }
        else {
            return [];
        }
    };
    if ($args->menu === 'Main Menu') {
        $roles = $get_current_user_roles();
        $display = '';
        if (
                in_array('administrator', $roles) ||
                in_array('editor', $roles) ||
                in_array('author', $roles) ||
                in_array('job_seeker', $roles) &&
                in_array('job_lister', $roles)
        ) {
            $display = 'dual';
        }
        elseif (in_array('job_seeker', $roles)) {
            $display = 'job_seeker';
        }
        elseif (in_array('job_lister', $roles)) {
            $display = 'job_lister';
        }
        else {
            $display = 'guest';
        }
        $templateArgs['display'] = $display;
        ob_start();
        get_template_part('templates/menu-main', null, $templateArgs);
        $items = ob_get_clean();
    }

    return $items;
}, 10, 2);

add_filter('retrieve_password_message', function($message, $key, $user_login, $user_data) {
    $link = network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' );

    $args = [
        'link' => $link,
        'user_login' => $user_login,
    ];

    ob_start();
    get_template_part('templates/email/email-user', 'retrieve-password', $args);
    return ob_get_clean();
}, 10, 4);

add_filter('retrieve_password_title', function($title) {
    return "VMA Job Center Password Request";
}, 10, 1);

add_filter('jr_email_user_new_user_custom', function($email, $user_id) {
    $user = get_user_by( 'id', $user_id );
    $args = [
        'user' => $user,
    ];
    ob_start();
    get_template_part('templates/email/email-user', 'new-user', $args);
    $email['message'] = ob_get_clean();
    $email['subject'] = "Thank you for registering, {$user->user_login}";

    return $email;
}, 10 , 2);

add_filter('jr_job_preview_fields', function($fields) {
    if (isset($fields['Salary'])) {
        $fields['Salary'] = str_replace('&#36; ', '', $fields['Salary']);
    }
    return $fields;
});