jQuery(function($) {
	if ($.isFunction( $.fn.footable)) {
        const $table = $('.page-template-tpl-dashboard')
            .find('.myjobs_section .footable');

        if ($table) {
            
            $viewsHead = $table.find('th:nth-of-type(4)').remove();
            $viewsRows = $table.find('td:nth-of-type(4)').remove();
            $table.footable();
        }

    }
});