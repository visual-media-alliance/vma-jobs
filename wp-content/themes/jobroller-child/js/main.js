jQuery(function($) {

    function resizeDiv() {
        vpw = $(window).width(), vph = $(window).height(), $(".scroller").css({
            height: vph + "px"
        })
    }

    function deviceType() {
        return $(window).innerWidth() < 768 ? "mobile" : $(window).innerWidth() < 1200 ? "tablet" : "desktop"
    }


    $("a.page-scroll[href*=#]:not([href=#])").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") || location.hostname == this.hostname) {
            var i = $(this.hash);
            if (i = i.length ? i : $("[name=" + this.hash.slice(1) + "]"), i.length) return $("html,body").animate({
                scrollTop: i.offset().top
            }, 700), !1
        }
    });

    $("#header").addClass("original").clone().insertAfter("#header").addClass("cloned").removeClass("original");

    $(window).scroll(function() {
        $(".fade-scroll").css("opacity", 1 - $(window).scrollTop() / 250), $(".fade-scroll").css("transform", "translateY(" + .4 * $(window).scrollTop() + "px)")
    });



    var i = document.querySelector("#header.cloned");
    window.location.hash && i.classList.add("slide--up")
    
    new Headroom(i, {
        tolerance: {
            down: 1,
            up: 1
        },
        offset: 205,
        classes: {
            initial: "slide",
            pinned: "slide--reset",
            unpinned: "slide--up"
        }
    }).init();

    
    if(window.matchMedia("(max-width: 2800px)").matches) {
        $(".menu-item-has-children a[href='#']").removeAttr("href").css("cursor", "pointer");
        $(document).scroll(function() {
            var i = $(this).scrollTop();
            if(300 > i) {
                $("#header.original").fadeIn();
            }
            else {
                $("#header.original").fadeOut();
                $("#header.cloned ").fadeIn();
            } 
        });
    }


    $(".navbar-nav li a").on("click", function(i) {
        $(this).addClass("open");
    });
    

    $('a[href="#toggle-search"], .vma-search .close-group-btn').on("click", function(i) {
        i.preventDefault();
        $(".vma-search").toggleClass("open"), $('a[href="#toggle-search"]').toggleClass("active");
        $("html").toggleClass("noscroll"), $('a[href="#toggle-search"]').closest("li").toggleClass("active");
        $(".vma-search").hasClass("open") && setTimeout(function() {
            $(".vma-search .form-control").focus()
        }, 100);
    })

    
    $(document).ready(function() {
        resizeDiv()
    }),
    
    window.onresize = function(i) {
        resizeDiv()
    };
    
    $(function() {
        $(".sub-menu").hide(), $(".mobile-nav li:has(ul)").addClass("down"), $(".desktop-nav li:has(ul)").on("mouseenter", function() {
            $(this).children("ul").slideDown(200)
        }), $(".desktop-nav li:has(ul)").on("mouseleave", function() {
            $(this).children("ul").slideUp(200)
        }), $(".mobile-nav li:has(ul)").click(function() {
            $(this).children("ul").slideToggle(200), $(".mobile-nav li:has(ul)").toggleClass("down")
        })
    });

    window.onhashchange = function() {
        window.location.reload();
    }

    /**
     * Hack to fix the in document back button from erasing user input
     * when the form action fires on step 4.
     */
    $('.submit_form.main_form input.goback').on('click', function(e) {
        const step = $('.submit_form.main_form')
            .find('input[name="step"]')
            .attr('value');
        if (step === '4') {
            e.preventDefault();
            window.history.back();
            return false;
        }
        return true;
    });

    /**
     * Remove tool tips from resume headings
     */
	if ($.isFunction( $.fn.qtip)) {
		$('h1.resume-title span, .resume_header img').qtip('destroy', true);
		$('ol.resumes li' ).qtip('destroy', true);
    }
    

});