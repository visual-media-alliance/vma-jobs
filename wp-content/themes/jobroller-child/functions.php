<?php

require __DIR__ . '/includes/template-functions.php';
require __DIR__ . '/includes/template-hooks.php';
require __DIR__ . '/includes/template-tags.php';


global $supportContact;

$supportContact = [
    'name' => 'Sue Benavente',
    'email' => 'sue@vma.bz',
    'phone' => '415-489-7622',
];

$compensationMetaArgs = [
    'relation' => 'OR',
    'sort-order' => [
        'key' => 'sort-order',
        'type' => 'NUMERIC',
    ],
    [
        'key' => 'sort-order',
        'compare' => 'NOT EXISTS'
    ],
];


$replacements = [
    'Add Resume' =>
        'Post Resume',
    'Desired salary: ' =>
        'Desired Compensation: ',
    'Desired Salary (only numeric values)' =>
        'Desired Compensation (indicate hourly/salary/commission)',
    'e.g. 25000' =>
        '$40 / Hour',
    'Latest Jobs' => 
        'Search Jobs',
    'Latest Jobs%s' => 
        'Search Jobs',
    'Telephone' =>
        'Phone',
    'Telephone including area code' =>
        'Phone number including area code',
    'The job has was Starred.' =>
        'The job has been saved.',
    'The job has was un-Starred.' =>
        'The job has been un-saved.',
    'Star Job' =>
        'Save Job',
    'Un-star Job' =>
        'Un-save Job',
    'Starred Jobs' =>
        'Saved Jobs',
    'You have not starred any jobs yet. You can star jobs from the individual job listing pages.' =>
        'You have not saved any jobs yet. You can save jobs from the individual job listing pages.',
    'Resume Location' =>
        'Your Location',
    'e.g. Lead Developer' =>
        'Print Production Manager',
    'Submit a Job' =>
        'Submit a Job Opening',
    'Your email' =>
        'Your Email',
    'Job Salary' =>
        'Job Compensation',
    'Jobs with a salary of %1$s %2$s' =>
        'Jobs with a compensation of %1$s %2$s',
];

$scope = new class {
    public function replaceGoogleApiKey(array $geoSettings)
    {
        $geoSettings['google']['api_key'] = ENV['google_apis_key'];
        return $geoSettings;
    }
};


$should_admin_bar = function () {
    return current_user_can('level_0');
};


add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/styles/bootstrap.min.css?include', ['at-main-css'], '1.0.0', 'all');
    // wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/styles/bootstrap.min.css?include');
    wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800', false );
    /** TODO: remove the original commented line original line bellow */
    /** REMOVELINES */
    // wp_enqueue_style( 'custom', get_stylesheet_directory_uri(). '/styles/custom.css', array('at-color-css'), '1.0.0','all' );
    wp_enqueue_style( 'custom', get_stylesheet_directory_uri(). '/styles/custom.css', array('at-main-css'), '1.0.0','all' );


    wp_enqueue_style('user', get_theme_file_uri('/styles/user.css'), [], filemtime(get_theme_file_path('/styles/user.css')));

    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', ['jquery'], '3.3.7', true);
    wp_enqueue_script('headroom', get_stylesheet_directory_uri() . '/js/libs/headroom.js?include', [], '4.0.2', true);
    wp_enqueue_script('parallax', get_stylesheet_directory_uri() . '/js/libs/parallax.js?include', ['jquery'], '1.5.0', true);
    wp_enqueue_script( 'footable-override', get_theme_file_uri('/js/footable-override.js'), ['jquery'], filemtime(get_theme_file_path('/js/footable-override.js')), false);
    wp_enqueue_script( 'main', get_theme_file_uri('/js/main.js'), ['jquery'], filemtime(get_theme_file_path('/js/main.js')), true);

    /** Needed by the old fancybox included in Job Roller parent */
    $patch = "
    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
    ";

    wp_add_inline_script('jquery', $patch);
    
});

/**
 * Remove confirm weak password from front-end login screens
 */
add_action('login_enqueue_scripts', function() {
    if (wp_get_environment_type() !== 'local') {
        return;
    }
	?>
		<script>
			document.addEventListener("DOMContentLoaded", function(event) { 
				var elements = document.getElementsByClassName('pw-weak');
				console.log(elements);
				var requiredElement = elements[0];
				requiredElement.remove();
			});
		</script>
	<?php
});

add_filter('option_jr_options', function($value) use ($scope)  {
    if (empty($value)) {
        return; $value;
    }

    $value['jr_nu_custom_email'] = true;

    $value['geocoder'] = 'google';
    $value['map_provider'] = 'google';
    /** This also needs to bet replace for the geocoder_settings "geocoder_settings" option key  */
    $value['geocoder_settings'] = $scope->replaceGoogleApiKey($value['geocoder_settings']);
     /** This also needs to bet replace for the geocoder_settings "map_provider_settings" option key  */
    $value['map_provider_settings'] = $scope->replaceGoogleApiKey($value['map_provider_settings']);

    $value['jr_disable_blog'] = 'yes';
    $value['jr_allow_registration_password'] = 'yes';
    $value['jr_jobs_default_expires'] = '30'; // days
    $value['jr_expired_action'] = 'hide';
    $value['jr_jobs_require_moderation'] = false;
    $value['jr_editing_needs_approval'] = false;
    $value['jr_resume_listing_visibility'] = 'listers';
    $value['jr_resume_visibility'] = 'listers';
    $value['jr_resume_require_subscription'] = false;
    $value['jr_resume_show_contact_form'] = false;

    #$value['jr_admin_security'] = 'edit_others_posts';
    $value['jr_remove_admin_bar'] = false;
    $value['jr_ad_stats_all'] = false;


    return $value;
});

add_filter('geocoder_settings', function($value) use ($scope) {
    $value = $scope->replaceGoogleApiKey($value);
});

add_filter('map_provider_settings', function($value) use ($scope) {
    $value = $scope->replaceGoogleApiKey($value);
});


add_filter('gettext_jobroller', function($translation, $text) use($replacements) {
    if (array_key_exists($text, $replacements)) {
        return $replacements[$translation];
    }
    return $translation;
}, 10, 2);


/**
 * Show the admin bar only for select user caps
 */
add_filter('show_admin_bar', function($show) use($should_admin_bar) {
    if (function_exists('current_user_switched')) {
        $switched_user = current_user_switched();
        if ($switched_user) {
            if ($switched_user->has_cap('level_0')) {
                return true;
            }
        }
    }

    if ($show) {
        return $should_admin_bar();
    }

    return $show;
});


/**
 * Force all links on Top Left Menu to open in new tab
 */
add_filter('wp_nav_menu_objects', function($sorted_menu_items, $args) {
    if ($args->menu !== 'Top Left Menu') {
        return;
    }
    foreach($sorted_menu_items as $item) {
        $item->target = "_blank";
    }
    return $sorted_menu_items;
}, 10, 2);


/**
 * Correct layout problems when admin bar is hidden when a user is logged in.
 */
add_filter('body_class', function($classes, $additional) use($should_admin_bar) {
    if (!$should_admin_bar()) {
        $classes[] = 'no-admin-bar';
    }
    return $classes;
}, 10, 2);



add_filter('registration_errors', function($errors) {
    if ($_POST['role'] === 'job_lister') {
        $errors->add(401, 'Invalid Role provided in user registration');
    }
    return $errors;
});

add_action('template_redirect', function() {
    if (is_page()) {
        $post = get_post();
        if ($post->ID === APP_Login::get_id()) {
            if (is_user_logged_in()) {
                wp_redirect(site_url());
                die();
            }
        }
    }
});

add_filter('after_setup_theme', function() {

    /** Remove the app theme headers so we can take control */
    APP_Mail_From::remove();

    /** 
     * takes arguments supplied to wp_mail() to detect if it's targeted
     * towards the site admin for monitoring job and resume activity by Job Roller theme
     */
    add_filter('wp_mail', function($args) {
        if (
            strpos($args['to'], get_option('admin_email')) !== false &&
            ENV['admin_email_sendall']
        ) {
            $accounts = array_filter(get_users(['role__in' => ['administrator']]), function($user) {
                if (get_the_author_meta('jr_no_admin_emails', $user->ID) === "1") {
                    return false;
                }
                return true;
            });

            $recipients = array_map(function($account) {
                return $account->user_email;
            }, $accounts);
        
            $args['to'] = implode(',', $recipients);
        }
    
        $headers = [
            'mime' => 'MIME-Version: 1.0',
            'type' => 'Content-Type: text/html; charset="' . get_bloginfo( 'charset' ) . '"',
        ];

        if (empty($args['headers'])) {
            $args['headers'] = $headers;
        }

        return $args;
    });

    add_filter( 'wp_mail_from', fn() => ENV['smtp_from']);
    add_filter( 'wp_mail_from_name', fn() => 'Visual Media Alliance');

    function test_mail() {
        $to = 'seclayer@gmail.com';
        $subject = 'The subject';
        $body = 'The email body content';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        add_action('wp_mail_failed', function($e) {
            echo $e->get_error_message();
        });

        return wp_mail( $to, $subject, $body, $headers );
    }
});



add_action('init', function() {

    $t = get_taxonomy('job_salary');
    $t->labels->name = 'Compensation';
    $t->labels->singular_name = 'Compensation';
    $t->labels->menu_name = 'Compensation';
    $t->labels->all_items = 'All Levels';
    $t->labels->parent_item                     = 'Parent Compensation Level';
    $t->labels->parent_item_colon               = 'Parent Compensation Level:';
    $t->labels->new_item_name                   = 'New Compensation Level';
    $t->labels->add_new_item                    = 'Add New Compensation Level';
    $t->labels->edit_item                       = 'Edit Compensation Level';
    $t->labels->update_item                     = 'Update Compensation Level';
    $t->labels->separate_items_with_commas      = 'Separate Compensation Levels with commas';
    $t->labels->search_items                    = 'Search Compensations Levels';
    $t->labels->add_or_remove_items             = 'Add or remove Compensation Levels';
    $t->labels->choose_from_most_used           = 'Choose from the most used Compensation Levels';
    $t->rewrite['slug'] = 'job-compensation';
    register_taxonomy( 'job_salary', 'job_listing', (array) $t);
}, 11);


add_filter('pre_get_terms', function($query) use($compensationMetaArgs) {
    $taxonomy = $query->query_vars['taxonomy'][0];
    if ($taxonomy !== 'job_salary') {
        return $query;
    }
    $query->query_vars['orderby'] = 'sort-order';
    $query->meta_query = new WP_META_QUERY($compensationMetaArgs);
    return $query;
});


add_action('admin_init', function() use($compensationMetaArgs) {
    $hideDescription = function() {
        ?><style>.term-description-wrap{display:none;}</style><?php
    };
    
    add_action("job_salary_edit_form", function( $tag, $taxonomy ) use($hideDescription)
    { 
        $hideDescription();
    }, 10, 2);
    
    add_action("job_salary_add_form", function( $taxonomy ) use($hideDescription)
    { 
        $hideDescription();
    }, 10);
    
    add_action('job_salary_add_form_fields', function($tax) {
        ?>
        <div class="form-field sort-order">
            <label for="sort-order">Sort Order</label>
            <input class="postform" type="number" id="sort-order" name="sort-order" value="" size="5" />
        </div>
        <?php
    });
    
    add_action('created_job_salary', function($term_id) {
        $input = ($_POST['sort-order'] ?? null);
        if ($input !== null) {
            $number = (int) $input;
            add_term_meta($term_id, 'sort-order', $number, true);
        }
    });
    
    add_action('job_salary_edit_form_fields', function($term, $tax) {
        $number = get_term_meta( $term->term_id, 'sort-order', true);
        ?>
        <tr class="form-field term-group-wrap">
        <th scope="row"><label for="sort-order">Sort Order</label></th>
            <td><input
                class="postform"
                type="number"
                id="sort-order"
                name="sort-order" 
                value="<?= $number ?>"
                /></td>
        </tr>
        <?php

    /**
     * Remove confirm weak password in user edit screen
     */
    add_action('admin_head', function() {
        if (wp_get_environment_type() !== 'local') {
            return;
        }
        ?>
            <script>
                document.addEventListener("DOMContentLoaded", function(event) { 
                    var elements = document.getElementsByClassName('pw-weak');
                    console.log(elements);
                    var requiredElement = elements[0];
                    requiredElement.remove();
                });
            </script>
        <?php
    });

    }, 10, 2);
    
    add_action('edited_job_salary', function($term_id) {
        $input = ($_POST['sort-order'] ?? "");
        if ($input !== "") {
            $number = (int) $input;
            update_term_meta($term_id, 'sort-order', $number);
        }
        else {
            delete_term_meta($term_id, 'sort-order');
        }
    });
    
    add_filter('manage_edit-job_salary_columns', function($columns) {
        unset($columns['description']);
        $columns['sort-order'] = 'Sort Order';
        return $columns;
    });
    
    add_filter('manage_job_salary_custom_column', function($content, $column_name, $term_id) {
        if( $column_name !== 'sort-order' ){
            return $content;
        }
        return get_term_meta($term_id, 'sort-order', true);
    }, 10 , 3);
    
    add_filter( 'manage_edit-job_salary_sortable_columns', function($sortable) {
        $sortable['sort-order'] = 'sort-order';
        return $sortable;
    });
    
    add_filter('pre_get_terms', function($query)  use($compensationMetaArgs) {
        global $pagenow;

        $modifyQuery = (
            $pagenow == 'edit-tags.php' &&
            $query->query_vars['taxonomy'][0] === 'job_salary' &&
            isset($_GET['orderby']) &&
            $_GET['orderby'] == 'sort-order'
        );

        if (!$modifyQuery) {
            return $query;
        }

        //$query->meta_query = new WP_Meta_Query($compensationMetaArgs);

        return $query;
    }); 
    
    /**
     * Add Option to user edit screen to supress email notifications of user activity
     */
    $extra_user_profile_fields = function($user) { ?>

        <?php
            $roles = ( array ) $user->roles;
            if (!in_array('administrator', $roles)) {
                return;
            }
        ?>
        <h3>Extra profile information</h3>

        <table class="form-table">
        <tr>
            <th><label for="jr_no_admin_emails">Prevent User Activity Notifications</label></th>
            <td>
                <input 
                    type="checkbox" 
                    name="jr_no_admin_emails"
                    id="jr_no_admin_emails" 
                    <?php if(esc_attr(get_the_author_meta('jr_no_admin_emails', $user->ID)) === "1"): ?>
                        checked
                    <?php endif ?>
                    class="regular-text" 
                />
                <span class="description">Checking this will stop email notifications of user acitivity</span>
            </td>
        </tr>
        </table>
    <?php };

    add_action('show_user_profile', $extra_user_profile_fields);
    add_action('edit_user_profile', $extra_user_profile_fields);

    $save_extra_user_profile_fields = function($user_id) {
        if (empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id)) {
            return;
        }
        
        if (!current_user_can('edit_user', $user_id)) { 
            return false; 
        }
        update_user_meta($user_id, 'jr_no_admin_emails', isset($_POST['jr_no_admin_emails']));
    };

    add_action('personal_options_update', $save_extra_user_profile_fields);
    add_action('edit_user_profile_update', $save_extra_user_profile_fields);


});
