<?php
/**
 * Schema.org structured data Place type classes.
 *
 * @package JobRoller\StructuredData
 * @author AppThemes
 * @since 1.9.1
 */

/**
 * Place schema type.
 *
 * Entities that have a somewhat fixed, physical extension.
 *
 * @since 1.9.1
 */
class JR_Schema_Type_Place_Job extends APP_Schema_Type_Place {
	/**
	 * Constructor.
	 *
	 * @param WP_Post $post Post object to be used for building schema type.
	 */
	public function __construct( WP_Post $post = null ) {
		if ( ! $post ) {
			return;
		}

		$this->set( 'address', new JR_Schema_Type_PostalAddress_Job( $post ) );
	}

}
