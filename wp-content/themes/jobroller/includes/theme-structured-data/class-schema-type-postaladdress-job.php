<?php
/**
 * Schema.org structured data PostalAddress type classes.
 *
 * @package JobRoller\StructuredData
 * @author AppThemes
 * @since 1.9.1
 */

/**
 * Generates the PostalAddress type schema json-ld.
 *
 * @link  https://schema.org/PostalAddress
 * @link  https://developers.google.com/schemas/reference/types/PostalAddress
 *
 * @since 1.9.1
 */
class JR_Schema_Type_PostalAddress_Job extends APP_Schema_Type_PostalAddress {

	/**
	 * Constructor.
	 *
	 * @param WP_Post $post Post object to be used for building schema type.
	 */
	public function __construct( WP_Post $post = null ) {
		if ( ! $post ) {
			return;
		}

		$region = explode( ',', $post->geo_short_address_country );

		$this
			->set( 'streetAddress', '' )
			->set( 'postalCode', '' )
			->set( 'addressLocality', $post->geo_short_address )
			->set( 'addressRegion', trim( $region[0] ) )
			->set( 'addressCountry', $post->geo_country );
	}

}
