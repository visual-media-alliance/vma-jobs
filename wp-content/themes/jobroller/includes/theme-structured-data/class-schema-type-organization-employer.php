<?php
/**
 * Schema.org structured data Employer Organization type classes.
 *
 * @package JobRoller\StructuredData
 * @author AppThemes
 * @since 1.9.1
 */

/**
 * Generates the Organization type schema json-ld.
 *
 * @link  https://schema.org/Organization
 *
 * @since 1.9.1
 */
class JR_Schema_Type_Organization_Employer extends APP_Schema_Type_Organization {

	/**
	 * Constructor.
	 *
	 * @param WP_Post $post Post object to be used for building schema type.
	 */
	public function __construct( WP_Post $post = null ) {
		if ( ! $post ) {
			return;
		}

		if ( ! $post->_Company ) {
			return;
		}

		$this
			->set( 'name', esc_html( $post->_Company ) )
			->set( 'sameAs', esc_url( $post->_CompanyURL ) )
			->set( 'url', esc_url( $post->_CompanyURL ) )
			->set( 'logo', new APP_Schema_Type_ImageObject_Attachment( $post ) );
	}

}
