<?php
/**
 * Schema.org structured data JobPosting type classes.
 *
 * @package JobRoller\StructuredData
 * @author AppThemes
 * @since 1.9.1
 */

/**
 * Generates the JobPosting type schema json-ld.
 *
 * @link  https://schema.org/JobPosting
 * @link  https://developers.google.com/schemas/reference/types/JobPosting
 *
 * @since 1.9.1
 */
class JR_Schema_Type_JobPosting_Job extends APP_Schema_Type_JobPosting_Post {

	/**
	 * Constructor.
	 *
	 * @param WP_Post $post Post object to be used for building schema type.
	 */
	public function __construct( WP_Post $post = null ) {
		if ( ! $post ) {
			return;
		}

		parent::__construct( $post );

		// Get all the salaries.
		$sals       = get_the_terms( $post->ID, 'job_salary' );
		$sals       = is_array( $sals ) ? $sals : array();
		$sals_names = wp_list_pluck( $sals, 'name' );
		$sals_names = implode( ', ', $sals_names );

		// Employment type.
		$job_types      = get_the_terms( $post->ID, 'job_type' );
		$job_types      = is_array( $job_types ) ? $job_types : array();
		$job_type_names = wp_list_pluck( $job_types, 'name' );
		$job_type_names = implode( ', ', $job_type_names );

		// Category.
		$job_cats      = get_the_terms( $post->ID, 'job_cat' );
		$job_cats      = is_array( $job_cats ) ? $job_cats : array();
		$job_cat_names = wp_list_pluck( $job_cats, 'name' );
		$job_cat_names = implode( ', ', $job_cat_names );

		if ( $post->geo_address ) {
			$this->set( 'jobLocation', new JR_Schema_Type_Place_Job( $post ) );
		} else {
			$this->set( 'jobLocationType', 'TELECOMMUTE' );
		}

		$this
			->set( 'hiringOrganization', new JR_Schema_Type_Organization_Employer( $post ) )
			->set( 'employmentType', $job_type_names )
			->set( 'occupationalCategory', $job_cat_names )
			->set( 'estimatedSalary', $sals_names );

		$duration = get_post_meta( $post->ID, JR_JOB_DURATION_META, true );
		if ( ! empty( $duration ) ) {
			$expiration_date = date( DATE_ISO8601, strtotime( $post->post_date . ' + ' . $duration . 'days' ) );
			$this->set( 'validThrough', $expiration_date );
		}
	}

}
