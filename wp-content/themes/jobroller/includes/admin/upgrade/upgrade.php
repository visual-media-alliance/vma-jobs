<?php
/**
 * Upgrade procedures.
 *
 * @todo Include upgrade procedures prior to 1.9.0
 *
 * @package JobRoller
 * @author  AppThemes
 * @since   1.9.0
 */

class JR_Upgrade_Theme_Options extends APP_Progress_Upgrade_Step {

	/**
	 * Retrieves the items array to be processed.
	 *
	 * @global scbOptions $jr_options
	 *
	 * @return array
	 */
	protected function _get_items() {
		return array();
	}

	/**
	 * Retrieves total number of items to be processed withing current step.
	 *
	 * @return int
	 */
	public function get_total() {
		return count( $this->_get_items() );
	}

	/**
	 * Processes items.
	 *
	 * @param APP_Dynamic_Checkout $checkout Checkout object.
	 *
	 * @return int The number of actually processed items.
	 */
	public function progress( APP_Dynamic_Checkout $checkout ) {
		global $jr_options;
		$items = $this->_get_items();
		$jr_options->update( $items );
		return count( $items );
	}

}

class JR_Upgrade_Theme_Options_1_9_0 extends JR_Upgrade_Theme_Options {

	/**
	 * Retrieves the items array to be processed.
	 *
	 * @global scbOptions $jr_options
	 *
	 * @return array
	 */
	protected function _get_items() {
		global $jr_options;

		return array(
			'geo_unit'              => $jr_options->jr_distance_unit ? $jr_options->jr_distance_unit : 'mi',
			'default_radius'        => '',
			'marker_color'          => '#565656',
			'geocoder'              => 'google',
			'map_provider'          => 'google',
			'geocoder_settings'     => array(
				'google' => array(
					'geo_region'   => $jr_options->jr_gmaps_region ? $jr_options->jr_gmaps_region : 'us',
					'geo_language' => $jr_options->jr_gmaps_lang ? $jr_options->jr_gmaps_lang : 'en',
					'geo_unit'     => $jr_options->jr_distance_unit ? $jr_options->jr_distance_unit : 'mi',
					'api_key'      => $jr_options->gmaps_api_key,
				),
			),
			'map_provider_settings' => array(
				'google' => array(
					'geo_region'   => $jr_options->jr_gmaps_region ? $jr_options->jr_gmaps_region : 'us',
					'geo_language' => $jr_options->jr_gmaps_lang ? $jr_options->jr_gmaps_lang : 'en',
					'geo_unit'     => $jr_options->jr_distance_unit ? $jr_options->jr_distance_unit : 'mi',
					'color_scheme' => 'standard',
					'api_key'      => $jr_options->gmaps_api_key,
				),
			),
		);
	}
}

class JR_Upgrade_Geodata_1_9_0 extends APP_Progress_Upgrade_Step {

	/**
	 * Retrieves total number of items to be processed withing current step.
	 *
	 * @return int
	 */
	public function get_total() {
		global $wpdb;

		$sql = "";
		$sql .= "SELECT COUNT(pm.post_id) ";
		$sql .= "FROM {$wpdb->postmeta} pm ";
		$sql .= "LEFT JOIN $wpdb->app_geodata gd ON (gd.post_id = pm.post_id) ";
		$sql .= "WHERE gd.post_id IS NULL AND pm.meta_key = '_jr_geo_latitude'";

		$totalposts = intval( $wpdb->get_var( $sql ) );
		return $totalposts;
	}

	/**
	 * Processes items.
	 *
	 * @param APP_Dynamic_Checkout $checkout Checkout object.
	 *
	 * @global wpdb $wpdb
	 *
	 * @return int The number of actually processed items.
	 */
	public function progress( APP_Dynamic_Checkout $checkout ) {
		global $wpdb;

		$limit = 1000;
		$count = 0;

		$sql = "";
		$sql .= "SELECT pmlat.post_id as post_id, pmlat.meta_value as lat, pmlng.meta_value as lng ";
		$sql .= "FROM {$wpdb->postmeta} pmlat ";
		$sql .= "LEFT JOIN {$wpdb->postmeta} pmlng ON (pmlat.post_id = pmlng.post_id AND pmlng.meta_key = '_jr_geo_longitude') ";
		$sql .= "LEFT JOIN {$wpdb->app_geodata} gd ON (gd.post_id = pmlat.post_id) ";
		$sql .= "WHERE gd.post_id IS NULL AND pmlat.meta_key = '_jr_geo_latitude' ";
		$sql .= "LIMIT {$limit}";

		$geodata = $wpdb->get_results( $sql );

		foreach ( $geodata as $row ) {
			$count += appthemes_set_coordinates( $row->post_id, $row->lat, $row->lng );
		}

		return $count;
	}
}
