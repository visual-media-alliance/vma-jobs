<?php
/**
 * Custom post types and taxonomies
 *
 * @version 1.8
 * @author AppThemes
 * @package JobRoller\Post-Types
 * @copyright 2010 all rights reserved
 */

add_action( 'init', 'jr_post_type', 10 );

add_filter( 'request', 'jr_rss_request' );
add_filter( 'pre_get_posts', 'jr_rss_pre_get_posts' );


### Hook Callbacks.

/**
 * Create the custom post type and category taxonomy for job listings.
 */
function jr_post_type() {
	global $jr_options;

	// make sure the new roles are added to the DB before registering the post types
	if ( isset( $_GET['firstrun'] ) ) {
		jr_init_roles();
	}

	// get the slug value for the ad custom post type & taxonomies
	if ( $jr_options->jr_job_permalink ) {
		$post_type_base_url = $jr_options->jr_job_permalink;
	} else {
		$post_type_base_url = 'jobs';
	}

	if ( $jr_options->jr_job_cat_tax_permalink ) {
		$cat_tax_base_url = $jr_options->jr_job_cat_tax_permalink;
	} else {
		$cat_tax_base_url = 'job-category';
	}

	if ( $jr_options->jr_job_type_tax_permalink ) {
		$type_tax_base_url = $jr_options->jr_job_type_tax_permalink;
	} else {
		$type_tax_base_url = 'job-type';
	}

	if ( $jr_options->jr_job_tag_tax_permalink ) {
		$tag_tax_base_url = $jr_options->jr_job_tag_tax_permalink;
	} else {
		$tag_tax_base_url = 'job-tag';
	}

	if ( $jr_options->jr_job_salary_tax_permalink ) {
		$sal_tax_base_url = $jr_options->jr_job_salary_tax_permalink;
	} else {
		$sal_tax_base_url = 'salary';
	}

	if ( $jr_options->jr_resume_permalink ) {
		$resume_post_type_base_url = $jr_options->jr_resume_permalink;
	} else {
		$resume_post_type_base_url = 'resumes';
	}


	// register the new job category taxonomy
	$labels = array(
		'name'                       => _x( 'Job Categories', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Job Category', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Job Categories', APP_TD ),
		'popular_items'              => __( 'Popular Job Categories', APP_TD ),
		'all_items'                  => __( 'All Job Categories', APP_TD ),
		'parent_item'                => __( 'Parent Job Category', APP_TD ),
		'parent_item_colon'          => __( 'Parent Job Category:', APP_TD ),
		'edit_item'                  => __( 'Edit Job Category', APP_TD ),
		'view_item'                  => __( 'View Job Category', APP_TD ),
		'update_item'                => __( 'Update Job Category', APP_TD ),
		'add_new_item'               => __( 'Add New Job Category', APP_TD ),
		'new_item_name'              => __( 'New Job Category Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate job categories with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove job categories', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used job categories', APP_TD ),
		'not_found'                  => __( 'No job categories found.', APP_TD ),
		'no_terms'                   => __( 'No job categories', APP_TD ),
		'items_list_navigation'      => __( 'Job categories list navigation', APP_TD ),
		'items_list'                 => __( 'Job categories list', APP_TD ),
		'menu_name'                  => _x( 'Categories', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => $cat_tax_base_url, 'hierarchical' => true ),
	);
	register_taxonomy( APP_TAX_CAT, array( APP_POST_TYPE ), $args );

	// register the new job type taxonomy
	$labels = array(
		'name'                       => _x( 'Job Types', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Job Type', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Job Types', APP_TD ),
		'popular_items'              => __( 'Popular Job Types', APP_TD ),
		'all_items'                  => __( 'All Job Types', APP_TD ),
		'parent_item'                => __( 'Parent Job Type', APP_TD ),
		'parent_item_colon'          => __( 'Parent Job Type:', APP_TD ),
		'edit_item'                  => __( 'Edit Job Type', APP_TD ),
		'view_item'                  => __( 'View Job Type', APP_TD ),
		'update_item'                => __( 'Update Job Type', APP_TD ),
		'add_new_item'               => __( 'Add New Job Type', APP_TD ),
		'new_item_name'              => __( 'New Job Type Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate job types with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove job type', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used job types', APP_TD ),
		'not_found'                  => __( 'No job types found.', APP_TD ),
		'no_terms'                   => __( 'No job types', APP_TD ),
		'items_list_navigation'      => __( 'Job types list navigation', APP_TD ),
		'items_list'                 => __( 'Job types list', APP_TD ),
		'menu_name'                  => _x( 'Types', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => true,
		'show_ui'               => true,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => $type_tax_base_url, 'hierarchical' => true ),
	);
	register_taxonomy( APP_TAX_TYPE, array( APP_POST_TYPE ), $args	);

	// register the new job tag taxonomy
	$labels = array(
		'name'                       => _x( 'Job Tags', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Job Tag', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Job Tags', APP_TD ),
		'popular_items'              => __( 'Popular Job Tags', APP_TD ),
		'all_items'                  => __( 'All Job Tags', APP_TD ),
		'parent_item'                => __( 'Parent Job Tag', APP_TD ),
		'parent_item_colon'          => __( 'Parent Job Tag:', APP_TD ),
		'edit_item'                  => __( 'Edit Job Tag', APP_TD ),
		'view_item'                  => __( 'View Job Tag', APP_TD ),
		'update_item'                => __( 'Update Job Tag', APP_TD ),
		'add_new_item'               => __( 'Add New Job Tag', APP_TD ),
		'new_item_name'              => __( 'New Job Tag Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate job tags with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove job tag', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used job tags', APP_TD ),
		'not_found'                  => __( 'No job tags found.', APP_TD ),
		'no_terms'                   => __( 'No job tags', APP_TD ),
		'items_list_navigation'      => __( 'Job tags list navigation', APP_TD ),
		'items_list'                 => __( 'Job tags list', APP_TD ),
		'menu_name'                  => _x( 'Tags', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => false,
		'show_ui'               => true,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => $tag_tax_base_url ),
	);
	register_taxonomy( APP_TAX_TAG, array( APP_POST_TYPE ), $args );

	// register the salary taxonomy
	$labels = array(
		'name'                       => _x( 'Salaries', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Salary', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Salaries', APP_TD ),
		'popular_items'              => __( 'Popular Salaries', APP_TD ),
		'all_items'                  => __( 'All Salaries', APP_TD ),
		'parent_item'                => __( 'Parent Salary', APP_TD ),
		'parent_item_colon'          => __( 'Parent Salary:', APP_TD ),
		'edit_item'                  => __( 'Edit Salary', APP_TD ),
		'view_item'                  => __( 'View Salary', APP_TD ),
		'update_item'                => __( 'Update Salary', APP_TD ),
		'add_new_item'               => __( 'Add New Salary', APP_TD ),
		'new_item_name'              => __( 'New Salary', APP_TD ),
		'separate_items_with_commas' => __( 'Separate salaries with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove salary', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used salaries', APP_TD ),
		'not_found'                  => __( 'No salaries found.', APP_TD ),
		'no_terms'                   => __( 'No salaries', APP_TD ),
		'items_list_navigation'      => __( 'Salaries list navigation', APP_TD ),
		'items_list'                 => __( 'Salaries list', APP_TD ),
		'menu_name'                  => _x( 'Salaries', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'       => $labels,
		'hierarchical' => true,
		'show_ui'      => true,
		'query_var'    => true,
		'rewrite'      => array( 'slug' => $sal_tax_base_url ),
	);
	register_taxonomy( APP_TAX_SALARY, array( APP_POST_TYPE ), $args );


	$custom_caps = array(
		'edit_posts' => 'edit_jobs', // enables job listers to view pending jobs
	);

	// create the custom post type for job listings
	$labels = array(
		'name'                  => _x( 'Jobs', 'post type general name', APP_TD ),
		'singular_name'         => _x( 'Job', 'post type singular name', APP_TD ),
		'add_new'               => __( 'Add New', APP_TD ),
		'add_new_item'          => __( 'Add New Job', APP_TD ),
		'edit'                  => __( 'Edit', APP_TD ),
		'edit_item'             => __( 'Edit Job', APP_TD ),
		'new_item'              => __( 'New Job', APP_TD ),
		'view'                  => __( 'View Jobs', APP_TD ),
		'view_item'             => __( 'View Job', APP_TD ),
		'view_items'            => __( 'View Jobs', APP_TD ),
		'search_items'          => __( 'Search Jobs', APP_TD ),
		'not_found'             => __( 'No jobs found.', APP_TD ),
		'not_found_in_trash'    => __( 'No jobs found in trash.', APP_TD ),
		'parent'                => __( 'Parent Job', APP_TD ),
		'parent_item_colon'     => __( 'Parent Job:', APP_TD ),
		'menu_name'             => _x( 'Jobs', 'post type menu name', APP_TD ),
		'all_items'             => __( 'All Jobs', APP_TD ),
		'archives'              => __( 'Job Archives', APP_TD ),
		'attributes'            => __( 'Job Attributes', APP_TD ),
		'insert_into_item'      => __( 'Insert into job', APP_TD ),
		'uploaded_to_this_item' => __( 'Uploaded to this job', APP_TD ),
		'featured_image'        => __( 'Featured Image', APP_TD ),
		'set_featured_image'    => __( 'Set featured image', APP_TD ),
		'remove_featured_image' => __( 'Remove featured image', APP_TD ),
		'use_featured_image'    => __( 'Use as featured image', APP_TD ),
		'filter_items_list'     => __( 'Filter jobs list', APP_TD ),
		'items_list_navigation' => __( 'Jobs list navigation', APP_TD ),
		'items_list'            => __( 'Jobs list', APP_TD ),
	);
	$args = array(
		'labels'              => $labels,
		'description'         => __( 'This is where you can create new job listings on your site.', APP_TD ),
		'public'              => true,
		'show_ui'             => true,
		'capabilities'        => $custom_caps,
		'map_meta_cap'        => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'menu_position'       => 8,
		'has_archive'         => true,
		'menu_icon'           => 'dashicons-portfolio',
		'hierarchical'        => false,
		'rewrite'             => array( 'slug' => $post_type_base_url, 'with_front' => false ), /* Slug set so that permalinks work when just showing post name */
		'query_var'           => true,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky' ),
	);
	register_post_type( APP_POST_TYPE, $args );


	if ( $jr_options->jr_allow_job_seekers ) {
		$show_ui = true;
	} else {
		$show_ui = false;
	}

	// register the resume category taxonomy
	$labels = array(
		'name'                       => _x( 'Resume Categories', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Resume Category', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Resume Categories', APP_TD ),
		'popular_items'              => __( 'Popular Resume Categories', APP_TD ),
		'all_items'                  => __( 'All Resume Categories', APP_TD ),
		'parent_item'                => __( 'Parent Resume Category', APP_TD ),
		'parent_item_colon'          => __( 'Parent Resume Category:', APP_TD ),
		'edit_item'                  => __( 'Edit Resume Category', APP_TD ),
		'view_item'                  => __( 'View Resume Category', APP_TD ),
		'update_item'                => __( 'Update Resume Category', APP_TD ),
		'add_new_item'               => __( 'Add New Resume Category', APP_TD ),
		'new_item_name'              => __( 'New Resume Category Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate resume categories with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove resume category', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used resume categories', APP_TD ),
		'not_found'                  => __( 'No resume categories found.', APP_TD ),
		'no_terms'                   => __( 'No resume categories', APP_TD ),
		'items_list_navigation'      => __( 'Resume categories list navigation', APP_TD ),
		'items_list'                 => __( 'Resume categories list', APP_TD ),
		'menu_name'                  => _x( 'Categories', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => true,
		'show_ui'               => $show_ui,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => 'resume/category', 'with_front' => false ),
	);
	register_taxonomy( APP_TAX_RESUME_CATEGORY, array( APP_POST_TYPE_RESUME ), $args );

	// register the resume job type taxonomy
	$labels = array(
		'name'                       => _x( 'Resume Job Types', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Resume Job Type', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Resume Job Types', APP_TD ),
		'popular_items'              => __( 'Popular Resume Job Types', APP_TD ),
		'all_items'                  => __( 'All Resume Job Types', APP_TD ),
		'parent_item'                => __( 'Parent Resume Job Type', APP_TD ),
		'parent_item_colon'          => __( 'Parent Resume Job Type:', APP_TD ),
		'edit_item'                  => __( 'Edit Resume Job Type', APP_TD ),
		'view_item'                  => __( 'View Resume Job Type', APP_TD ),
		'update_item'                => __( 'Update Resume Job Type', APP_TD ),
		'add_new_item'               => __( 'Add New Resume Job Type', APP_TD ),
		'new_item_name'              => __( 'New Resume Job Type Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate resume job types with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove resume type', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used resume job types', APP_TD ),
		'not_found'                  => __( 'No resume job types found.', APP_TD ),
		'no_terms'                   => __( 'No resume job types', APP_TD ),
		'items_list_navigation'      => __( 'Resume job types list navigation', APP_TD ),
		'items_list'                 => __( 'Resume job types list', APP_TD ),
		'menu_name'                  => _x( 'Types', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => false,
		'show_ui'               => $show_ui,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => 'resume/job-type', 'with_front' => false ),
	);
	register_taxonomy( APP_TAX_RESUME_JOB_TYPE, array( APP_POST_TYPE_RESUME ), $args );

	// register the resume specialties taxonomy
	$labels = array(
		'name'                       => _x( 'Resume Specialties', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Resume Specialty', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Resume Specialties', APP_TD ),
		'popular_items'              => __( 'Popular Resume Specialties', APP_TD ),
		'all_items'                  => __( 'All Resume Specialties', APP_TD ),
		'parent_item'                => __( 'Parent Resume Specialty', APP_TD ),
		'parent_item_colon'          => __( 'Parent Resume Specialty:', APP_TD ),
		'edit_item'                  => __( 'Edit Resume Specialty', APP_TD ),
		'view_item'                  => __( 'View Resume Specialty', APP_TD ),
		'update_item'                => __( 'Update Resume Specialty', APP_TD ),
		'add_new_item'               => __( 'Add New Resume Specialty', APP_TD ),
		'new_item_name'              => __( 'New Resume Specialty Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate resume specialties with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove resume specialty', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used resume specialties', APP_TD ),
		'not_found'                  => __( 'No resume specialties found.', APP_TD ),
		'no_terms'                   => __( 'No resume specialties', APP_TD ),
		'items_list_navigation'      => __( 'Resume specialties list navigation', APP_TD ),
		'items_list'                 => __( 'Resume specialties list', APP_TD ),
		'menu_name'                  => _x( 'Specialties', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => false,
		'show_ui'               => $show_ui,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => 'resume/speciality', 'with_front' => false ),
	);
	register_taxonomy( APP_TAX_RESUME_SPECIALITIES, array( APP_POST_TYPE_RESUME ), $args );

	// register the resume groups taxonomy
	$labels = array(
		'name'                       => _x( 'Resume Groups', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Resume Group', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Groups', APP_TD ),
		'popular_items'              => __( 'Popular Groups', APP_TD ),
		'all_items'                  => __( 'All Groups', APP_TD ),
		'parent_item'                => __( 'Parent Group', APP_TD ),
		'parent_item_colon'          => __( 'Parent Group:', APP_TD ),
		'edit_item'                  => __( 'Edit Group', APP_TD ),
		'view_item'                  => __( 'View Group', APP_TD ),
		'update_item'                => __( 'Update Group', APP_TD ),
		'add_new_item'               => __( 'Add New Group', APP_TD ),
		'new_item_name'              => __( 'New Group Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate resume groups with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove resume group', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used resume groups', APP_TD ),
		'not_found'                  => __( 'No resume groups found.', APP_TD ),
		'no_terms'                   => __( 'No resume groups', APP_TD ),
		'items_list_navigation'      => __( 'Resume groups list navigation', APP_TD ),
		'items_list'                 => __( 'Resume groups list', APP_TD ),
		'menu_name'                  => _x( 'Groups', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => false,
		'show_ui'               => $show_ui,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => 'resume/group', 'with_front' => false ),
	);
	register_taxonomy( APP_TAX_RESUME_GROUPS, array( APP_POST_TYPE_RESUME ), $args );

	// register the resume languages taxonomy
	$labels = array(
		'name'                       => _x( 'Resume Languages', 'taxonomy general name', APP_TD ),
		'singular_name'              => _x( 'Resume Langauge', 'taxonomy singular name', APP_TD ),
		'search_items'               => __( 'Search Resume Languages', APP_TD ),
		'popular_items'              => __( 'Popular Resume Languages', APP_TD ),
		'all_items'                  => __( 'All Resume Languages', APP_TD ),
		'parent_item'                => __( 'Parent Resume Language', APP_TD ),
		'parent_item_colon'          => __( 'Parent Resume Language:', APP_TD ),
		'edit_item'                  => __( 'Edit Resume Language', APP_TD ),
		'view_item'                  => __( 'View Resume Language', APP_TD ),
		'update_item'                => __( 'Update Resume Language', APP_TD ),
		'add_new_item'               => __( 'Add New Resume Language', APP_TD ),
		'new_item_name'              => __( 'New Resume Language Name', APP_TD ),
		'separate_items_with_commas' => __( 'Separate resume languages with commas', APP_TD ),
		'add_or_remove_items'        => __( 'Add or remove resume language', APP_TD ),
		'choose_from_most_used'      => __( 'Choose from the most used resume languages', APP_TD ),
		'not_found'                  => __( 'No resume languages found.', APP_TD ),
		'no_terms'                   => __( 'No resume languages', APP_TD ),
		'items_list_navigation'      => __( 'Resume languages list navigation', APP_TD ),
		'items_list'                 => __( 'Resume languages list', APP_TD ),
		'menu_name'                  => _x( 'Languages', 'taxonomy menu name', APP_TD ),
	);
	$args = array(
		'labels'                => $labels,
		'hierarchical'          => false,
		'show_ui'               => $show_ui,
		'query_var'             => true,
		'update_count_callback' => '_update_post_term_count',
		'rewrite'               => array( 'slug' => 'resume/language', 'with_front' => false ),
	);
	register_taxonomy( APP_TAX_RESUME_LANGUAGES, array( APP_POST_TYPE_RESUME ), $args );

	// create the custom post type for resumes
	$labels = array(
		'name'                  => _x( 'Resumes', 'post type general name', APP_TD ),
		'singular_name'         => _x( 'Resume', 'post type singular name', APP_TD ),
		'add_new'               => __( 'Add New', APP_TD ),
		'add_new_item'          => __( 'Add New Resume', APP_TD ),
		'edit'                  => __( 'Edit', APP_TD ),
		'edit_item'             => __( 'Edit Resume', APP_TD ),
		'new_item'              => __( 'New Resume', APP_TD ),
		'view'                  => __( 'View Resumes', APP_TD ),
		'view_item'             => __( 'View Resume', APP_TD ),
		'view_items'            => __( 'View Resumes', APP_TD ),
		'search_items'          => __( 'Search Resumes', APP_TD ),
		'not_found'             => __( 'No Resumes found.', APP_TD ),
		'not_found_in_trash'    => __( 'No Resumes found in trash.', APP_TD ),
		'parent'                => __( 'Parent Resume', APP_TD ),
		'parent_item_colon'     => __( 'Parent Resume:', APP_TD ),
		'menu_name'             => _x( 'Resumes', 'post type menu name', APP_TD ),
		'all_items'             => __( 'All Resumes', APP_TD ),
		'archives'              => __( 'Resume Archives', APP_TD ),
		'attributes'            => __( 'Resume Attributes', APP_TD ),
		'insert_into_item'      => __( 'Insert into resume', APP_TD ),
		'uploaded_to_this_item' => __( 'Uploaded to this resume', APP_TD ),
		'featured_image'        => __( 'Featured Image', APP_TD ),
		'set_featured_image'    => __( 'Set featured image', APP_TD ),
		'remove_featured_image' => __( 'Remove featured image', APP_TD ),
		'use_featured_image'    => __( 'Use as featured image', APP_TD ),
		'filter_items_list'     => __( 'Filter resumes list', APP_TD ),
		'items_list_navigation' => __( 'Resumes list navigation', APP_TD ),
		'items_list'            => __( 'Resumes list', APP_TD ),
	);
	$args = array(
		'labels'              => $labels,
		'description'         => __( 'Resumes are created and edited by job_seekers.', APP_TD ),
		'public'              => true,
		'show_ui'             => $show_ui,
		'capability_type'     => 'post',
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-id-alt',
		'hierarchical'        => false,
		'rewrite'             => array( 'slug' => $resume_post_type_base_url, 'with_front' => false ), /* Slug set so that permalinks work when just showing post name */
		'query_var'           => true,
		'has_archive'         => $resume_post_type_base_url,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' ),
	);
	register_post_type( APP_POST_TYPE_RESUME, $args );

}

/**
 * Get the custom taxonomy array and loop through the values.
 */
function jr_get_custom_taxonomy( $post_id, $tax_name, $tax_class ) {

	$tax_array = get_terms( $tax_name, array( 'hide_empty' => '0' ) );
	if ( $tax_array && sizeof( $tax_array ) > 0 ) {

		foreach ( $tax_array as $tax_val ) {
			if ( is_object_in_term( $post_id, $tax_name, array( $tax_val->term_id ) ) ) {
				echo '<span class="' . esc_attr( $tax_class . ' ' . $tax_val->slug ) . '">' . $tax_val->name . '</span>';
				break;
			}
		}

	}

}

/**
 * Add custom post types to the Main RSS feed.
 */
function jr_rss_request( $qv ) {
	if ( isset( $qv['feed'] ) && ! isset( $qv['post_type'] ) ) {
		$qv['post_type'] = array( 'post', 'job_listing' );
	}
	return $qv;
}

/**
 * Only retrieve published jobs in RSS feed.
 */
function jr_rss_pre_get_posts( $query ) {
	if ( $query->is_feed ) {
		$query->set( 'post_status', 'publish' );
	}
	return $query;
}
