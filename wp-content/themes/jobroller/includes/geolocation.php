<?php
/**
 * JobRoller Geoloaction functions
 * This file controls code for the Geolocation features.
 * Geolocation adapted from 'GeoLocation' plugin by Chris Boyd - http://geo.chrisboyd.net
 *
 * @version 1.1
 * @author AppThemes
 * @package JobRoller\Geolocation
 * @copyright 2010 all rights reserved
 */

define( 'JR_DEFAULT_ZOOM', 1 );

function jr_clean_coordinate($coordinate) {
	//$pattern = '/^(\-)?(\d{1,3})\.(\d{1,15})/';
	$pattern = '/^(\-)?(\d{1,3}).(\d{1,15})/';
	preg_match($pattern, $coordinate, $matches);
	if (isset($matches[0])) return $matches[0];
}

function jr_parse_address_components( $address_components ) {
	$address = $city = $country = $short_address = $short_address_country = $short_country = $state = '';

	$city = ! empty( $address_components['city'] ) ? $address_components['city'] : $city;
	$state = ! empty( $address_components['state'] ) ? $address_components['state'] : $state;
	$country = ! empty( $address_components['country'] ) ? $address_components['country'] : $country;
	$short_country = ! empty( $address_components['country_short'] ) ? $address_components['country_short'] : $short_country;

	if ( ($city != '') && ($state != '') && ($country != '') ) {
		$address = $city . ', ' . $state . ', ' . $country;
	} elseif ( ($city != '') && ($state != '') ) {
		$address = $city . ', ' . $state;
	} elseif( ($state != '') && ($country != '') ) {
		$address = $state . ', ' . $country;
	// fix for countries with no valid state
	} elseif( ($city != '') && ($country != '') ) {
		$address = $city . ', ' . $country;
	//
	} elseif ( $country != '' ) {
		$address = $country;
	}

	if ( $country=='United Kingdom' ) {
		$short_country = 'UK';
	}

	if ( ($city != '') && ($state != '') && ($country != '') ) {
		$short_address = $city;
		$short_address_country = $state . ', ' . $country;
	} else if ( ($city != '') && ($state != '') ) {
		$short_address = $city;
		$short_address_country = $state;
	} else if ( ($state != '') && ($country != '') ) {
		$short_address = $state;
		$short_address_country = $country;
		// fix for countries with no valid state
	} else if ( ($city != '') && ($country != '') ) {
		$short_address = $city;
		$short_address_country = $country;
		//
	} else if ( $country != '' ) {
		$short_address = $country;
		$short_address_country = '';
	}

	return array(
		'address' => $address,
		'country' => $country,
		'short_address' => $short_address,
		'short_address_country' => $short_address_country
	);
}

// Radial location search
function jr_radial_search( $location, $radius, $address_array = '' ) {

	if ( empty( $location ) ) {
		return false;
	}

	$lat    = 0;
	$lng    = 0;
	$radius = (int) $radius;

	if ( ! empty( $address_array['longitude'] ) && ! empty( $address_array['latitude'] ) ) {
		$lat = $address_array['latitude'];
		$lng = $address_array['longitude'];
	}

	$wp_query = new WP_Query( array(
		'post_type' => get_query_var( 'post_type' ),
		'lat'       => $lat,
		'lng'       => $lng,
		'radius'    => $radius,
		'location'  => $location,
		'orderby'   => 'distance',
		'order'     => 'ASC',
	) );

	$posts     = wp_list_pluck( $wp_query->posts, 'ID' );
	$geo_query = $wp_query->get( 'app_geo_query' );
	$result    = false;

	if ( $posts && ! empty( $geo_query ) ) {
		$radius = $geo_query['rad'];
		$result = array( 'address' => $location, 'radius' => $radius, 'posts' => $posts );
	}

	return $result;
}

// Shows the map on single job listings
function jr_job_map() {
	global $post;

	$title  = str_replace( '"', '&quot;', wptexturize( $post->post_title ) );
	$coords = appthemes_get_coordinates( $post->ID );

	if ( ! $coords->lat ) {
		return;
	}
	?>

	<div id="job_map" style="height: 300px; display:none; width:100%; overflow:hidden;"></div>
	<script type="text/javascript">
	/* <![CDATA[ */
		jQuery( function( $ ) {

			if ( ! $().appthemes_map ) {
				return;
			}

			// Slide Toggle
			$( 'a.toggle_map' ).click( function() {
				$( '#share_form' ).slideUp();
				$( '#apply_form' ).slideUp();
				$( '#job_map' ).slideToggle( 400, function() {
					data = {
						center_lat: <?php echo $coords->lat; ?>,
						center_lng: <?php echo $coords->lng; ?>,
						zoom: 10,
						auto_zoom: false,
						animation: true,
						markers: [{
							lat: <?php echo $coords->lat; ?>,
							lng: <?php echo $coords->lng; ?>,
							marker_text: '<?php echo $title; ?>'
						}]
					};

					$( '#job_map' ).appthemes_map( data );
				} );
				$( 'a.apply_online' ).removeClass( 'active' );
				$( this ).toggleClass( 'active' );
				return false;
			});

			} );
	/* ]]> */
	</script>
	<?php

}

function jr_radius_dropdown() {
	global $jr_options;
?>
	<div class="radius">
		<label for="radius"><?php _e('Radius:', APP_TD); ?></label>
		<select name="radius" class="radius" id="radius">
<?php
		$selected_radius = isset( $_GET['radius'] ) ? absint( $_GET['radius'] ) : 0;

		foreach ( array( 0, 1, 5, 10, 50, 100, 1000, 5000 ) as $radius ) {
			if ( ! $radius ) {
				$echo_radius = __( 'Auto', APP_TD );
			} else {
				$echo_radius = number_format_i18n( $radius ) . ' ' . $jr_options->geo_unit;
			}
			?><option value="<?php echo esc_attr($radius); ?>" <?php selected( $selected_radius, $radius ); ?>><?php echo $echo_radius; ?></option><?php
		}
?>
		</select>
	</div><!-- end radius -->
<?php
}

// clear cached locations to make sure geolocation changes are applied
function _jr_clear_geolocation_cache() {
	global $wpdb;

	$wpdb->query( "
		DELETE FROM $wpdb->options WHERE option_name LIKE '%_jr_geo_%'
	");
}

/**
 * Listing Geocode Address class.
 */
class JR_Listing_Geocode_Field {

	/**
	 * Current Listing type.
	 *
	 * @var string
	 */
	protected $ptype;

	/**
	 * The listing address form field name.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Additional meta fields to be saved with current field value and
	 * coordinates.
	 *
	 * The array keys correspond to keys of retrieved geocode data.
	 * The values correspond to meta field names to be saved in the database.
	 *
	 * @var array
	 */
	protected $fields = array();

	/**
	 * Constructs listing geocode address object.
	 *
	 * @param string $ptype  Listing post type.
	 * @param string $name   Address form field name.
	 */
	public function __construct( $ptype, $name = 'address' ) {

		$this->ptype  = $ptype;
		$this->name   = $name;
		$this->fields = array(
			'address'               => 'geo_address',
			'country'               => 'geo_country',
			'short_address'         => 'geo_short_address',
			'short_address_country' => 'geo_short_address_country',
		);

		add_action( "wp_ajax_{$this->ptype}_geocode_{$this->get_field_id()}_field", array( $this, 'handle_ajax' ) );
	}

	/**
	 * Validates field.
	 *
	 * Checks if address field was changed or never geocoded and try to update
	 * geocoder data before field save.
	 *
	 * @param mixed          $value Posted field value.
	 * @param scbCustomField $inst  Field object.
	 *
	 * @return mixed Validated value.
	 */
	public function validate( $value, $inst ) {

		$prev_value = get_post_meta( $inst->listing_id, $inst->name, true );
		$has_coords = appthemes_get_coordinates( $inst->listing_id, false );
		$data       = array();

		if ( $has_coords && $value === $prev_value ) {
			return $value;
		}

		$errors = ! isset( $inst->errors ) ? new WP_Error() : $inst->errors;

		$data_key = $inst->name . '_geo_data';

		if ( ! empty( $_POST[ $data_key ] ) ) {
			parse_str( $_POST[ $data_key ], $data );
			$data = wp_unslash( $data );
		}

		if ( ! trim( $value ) ) {
			$response = $this->delete_data( $inst->listing_id );
			if ( $has_coords && ! $response ) {
				$errors->add( 'cant_delete_coords', __( 'Could not delete geo data.', APP_TD ) );
			}
			return $value;
		}

		if ( empty( $data ) ) {

			$transient_key = 'jr_geo_coords_' . md5( $value );

			if ( ! defined( 'WP_DEBUG' ) || ! WP_DEBUG ) {
				$data = get_transient( $transient_key );
			}

			if ( ! $data ) {
				$data = $this->geocode_address( $value, $errors );

				if ( ! empty( $data['coords']['lat'] ) ) {
					// Cache for a week.
					set_transient( $transient_key, $data, 60 * 60 * 24 * 7 );
				}
			}
		}

		$data['address'] = $value;

		if ( empty( $data['lat'] ) && empty( $data['coords']['lng'] ) ) {
			$errors->add( 'cant_geocode_address', __( 'Could not geocode address.', APP_TD ) );
			if ( ! empty( $data['response_code'] ) ) {
				$errors->add( 'geocoder_error_code', sprintf( __( 'Geocoder returned with error code "%s".', APP_TD ), $data['response_code'] ) );
			}
			return $value;
		}

		$data = $this->set_data( $inst->listing_id, $data, $errors );

		if ( ! empty( $data['address'] ) ) {
			$value = $data['address'];
		}

		return $value;
	}

	/**
	 * Retrieves geocode data by address.
	 *
	 * @param string   $value  An address to geocode.
	 * @param WP_Error $errors Errors object.
	 *
	 * @return array Geodata.
	 */
	public function geocode_address( $value, $errors ) {
		$data = appthemes_geocode_address( $value );
		return json_decode( wp_json_encode( $data ), true );
	}

	/**
	 * Deletes geo data from database.
	 *
	 * @param int $listing_id Listing ID.
	 *
	 * @return int|bool Boolean False on failure.
	 */
	public function delete_data( $listing_id ) {
		foreach ( $this->get_geo_fields() as $name ) {
			delete_post_meta( $listing_id, $name );
		}
		return appthemes_delete_coordinates( $listing_id );
	}

	/**
	 * Set geo data.
	 *
	 * @param int      $listing_id Listing ID.
	 * @param array    $data       Geo data array to be saved.
	 * @param WP_Error $errors     Errors object.
	 */
	public function set_data( $listing_id, $data, $errors ) {
		$data['lat'] = ! empty( $data['coords']['lat'] ) ? $data['coords']['lat'] : ( ! empty( $data['lat'] ) ? $data['lat'] : 0 );
		$data['lng'] = ! empty( $data['coords']['lng'] ) ? $data['coords']['lng'] : ( ! empty( $data['lng'] ) ? $data['lng'] : 0 );

		// Address components can come as not formatted array, so we have to
		// re-generate it from raw data.
		if ( isset( $data['address_components'] ) && ! isset( $data['address_components']['country'] ) ) {
			$geocoder = APP_Geocoder_Registry::get_active_geocoder();
			if ( $geocoder ) {
				$data['address_components'] = $geocoder->parse_address_components( $data['address_components'] );
			}
		}

		$data = array_merge( $data, jr_parse_address_components( $data['address_components'] ) );

		$response = appthemes_set_coordinates( $listing_id, $data['lat'], $data['lng'] );

		if ( false === $response ) {
			$errors->add( 'cant_save_geodata', __( 'Could not save geo data.', APP_TD ) );
			return;
		}

		foreach ( $this->get_geo_fields() as $key => $name ) {
			// Some fields may refer to address components.
			if ( ! isset( $data[ $key ] ) && isset( $data['address_components'][ $key ] ) ) {
				$data[ $key ] = $data['address_components'][ $key ];
			}

			if ( isset( $data[ $key ] ) ) {
				update_post_meta( $listing_id, $name, $data[ $key ] );
			} else {
				delete_post_meta( $listing_id, $name );
			}
		}

		return $data;
	}

	/**
	 * Retrieves geo data from database.
	 *
	 * @param int $listing_id The listing ID.
	 *
	 * @return array
	 */
	public function get_data( $listing_id ) {
		$data = array();

		foreach ( $this->get_geo_fields() as $key => $value ) {
			$data[ $key ] = get_post_meta( $listing_id, $value, true );
		}

		$coords = appthemes_get_coordinates( $listing_id );
		$data = array_merge( $data, (array) $coords );

		return $data;
	}

	/**
	 * Field renderer.
	 *
	 * @param string   $value Field value (raw address).
	 * @param scbField $inst  Field object.
	 *
	 * @return string Generated HTML.
	 */
	public function render( $value, $inst ) {

		$field_id   = $this->get_field_id();
		$coord      = appthemes_get_coordinates( $inst->listing_id );
		$ajax_nonce = wp_create_nonce( "{$this->ptype}_geocode_{$field_id}_nonce" );
		$action     = "{$this->ptype}_geocode_{$field_id}_field";

		$inst->extra = ! empty( $inst->extra ) ? $inst->extra : array();

		$field = array(
			'type'      => 'text',
			'name'      => $inst->name,
			'title'     => $inst->title,
			'desc'      => $inst->desc,
			'wrap'      => $inst->wrap,
			'wrap_each' => $inst->wrap_each,
			'extra'     => array_merge( (array) $inst->extra, array(
				'id' => $field_id,
			) ),
		);

		ob_start();
		?>
		<div id="geolocation_box">
			<p>
				<?php
				echo scbForms::input_with_value( $field, $value );

				$height = ( is_admin() ) ? 200 : 350;
				$btn_text = ( is_admin() ) ? __( 'Find', APP_TD ) : __( 'Find Address/Location', APP_TD );

				?>
				<label>
					<input id="<?php echo esc_attr( $field_id ); ?>_find_on_map" type="button" class="button geolocationadd submit" style="display:none;" value="<?php echo esc_attr( $btn_text ); ?>">
				</label>
				<div id="<?php echo esc_attr( $field_id ); ?>_map_div" style="border:solid 2px #ddd;margin-top:5px;width:100%;height:<?php echo esc_attr( $height ); ?>px;position:relative;display:none;"></div>
				<input id="<?php echo esc_attr( $field_id ); ?>_lat" name="lat" type="hidden" value="<?php echo esc_attr( $coord->lat ); ?>" />
				<input id="<?php echo esc_attr( $field_id ); ?>_lng" name="lng" type="hidden" value="<?php echo esc_attr( $coord->lng ); ?>" />
				<input id="<?php echo esc_attr( $field_id ); ?>_geo_data" name="<?php echo esc_attr( $field_id ); ?>_geo_data" type="hidden" value="" />
			</p>
		</div>

		<script type="text/javascript">

			jQuery( function( $ ) {
				if ( !$().appthemes_map || !$().appAddressAutocomplete ) {
					return;
				}

				var
					input = $( "#<?php echo esc_js( $field_id ); ?>" ),
					id = input.attr( 'id' ),
					lat_input = $( "#"+id+"_lat" ),
					lng_input = $( "#"+id+"_lng" ),
					data_input = $( "#"+id+"_geo_data" );
					button = $( "#"+id+"_find_on_map" );

				input
					.appAddressAutocomplete( {
						ready: function() {
							var widget = $( this ).data( 'appthemesAppAddressAutocomplete' );
							if ( ! widget.api ) {
								var data = {};
								widget.getPlaceData = function() {
									return data;
								};
								button
									.click( function() {
										$.getJSON( AppThemes.ajaxurl, {
											action: '<?php echo $action; ?>',
											security: '<?php echo $ajax_nonce; ?>',
											address: input.val()
										}, function( response ) {
											data = {
												lat: response.coords.lat,
												lng: response.coords.lng,
												formatted_address: response.address,
												address_components: response.address_components
											};
											widget.onPlaceChange();
										} );
									} )
									.show();
							}
						}
					} )
					.on( 'appaddressautocompleteonplacechange', function() {
						var map = $('#'+id+'_map_div').data( 'appthemesAppthemes_map' );
						map.update_marker_position( {
							lat: lat_input.val(),
							lng: lng_input.val()
						} );
					} )
					.on( 'appaddressautocompletepopulateinputs', function( e, data ) {
						data_input.val( decodeURIComponent( $.param( data ) ) );
					} );

				$('#'+id+'_map_div').show().appthemes_map( {
					markers: [ {
						lat : +lat_input.val(),
						lng : +lng_input.val(),
						draggable : ( function() {
							// Don't make marker draggable If drag and drop is not supported by map provider.
							return $.appthemes.appthemes_map.prototype._marker_drag_end.toString() !== 'function( marker ) {}';
						} )()
					} ],
					zoom: 9,
					center_lat: +lat_input.val(),
					center_lng: +lng_input.val(),
					marker_drag_end: function( marker, lat, lng ) {
						lat_input.val( lat );
						lng_input.val( lng );

						$.getJSON( AppThemes.ajaxurl, {
							action: '<?php echo $action; ?>',
							security: '<?php echo $ajax_nonce; ?>',
							lat: lat,
							lng: lng
						}, function( response ) {
							data_input.val( decodeURIComponent( $.param( response ) ) );
							if( response.address ) {
								input.val( response.address );
							}
						} );
					}
				} );
			} );
		</script>
		<?php

		return ob_get_clean();
	}

	/**
	 * Handles AJAX actions.
	 */
	public function handle_ajax() {
		check_ajax_referer( "{$this->ptype}_geocode_{$this->get_field_id()}_nonce", 'security' );

		if ( isset( $_GET['address'] ) ) {
			$api_response = appthemes_geocode_address( sanitize_text_field( $_GET['address'] ) );
		} elseif ( isset( $_GET['lat'] ) ) {
			$api_response = appthemes_geocode_lat_lng( floatval( $_GET['lat'] ), floatval( $_GET['lng'] ) );
		}

		if ( ! $api_response ) {
			die( "error" );
		}

		die( json_encode( $api_response ) );
	}

	/**
	 * Retrieves address field name.
	 *
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * Retrieves address field name.
	 *
	 * @return string
	 */
	public function get_field_id() {
		return implode( '_', (array) $this->get_name() );
	}

	/**
	 * Retrives additional meta fields to be saved with current field value and
	 * coordinates.
	 *
	 * @return array
	 */
	public function get_geo_fields() {
		return apply_filters( 'appthemes_listing_geo_fields', $this->fields, $this->get_name(), $this->ptype );
	}
}

class JR_Job_Geocode_Field extends JR_Listing_Geocode_Field {
	public function __construct() {
		parent::__construct( APP_POST_TYPE, '_jr_address' );
	}
}

class JR_Resume_Geocode_Field extends JR_Listing_Geocode_Field {
	public function __construct() {
		parent::__construct( APP_POST_TYPE_RESUME, '_jr_address' );
	}
}
