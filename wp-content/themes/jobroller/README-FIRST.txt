JobRoller - Premium WordPress Theme


You can find the entire installation guide online.
https://www.appthemes.com/support/docs/

For help and support, please post your questions in our forum.
https://forums.appthemes.com/



Enjoy your new premium theme!


Your AppThemes Team
https://www.appthemes.com

** Please read the changelog.txt file for information on version modifications. **


Attributions
---------------------
Social balloon icons provided by Double-J Design - http://www.doublejdesign.co.uk

